package org.nurnix.kafka.producer.main;

public class ProducerOptions {

    private String topic;

    public ProducerOptions(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    @Override
    public String toString() {
        return "ProducerOptions{" +
                "topic='" + topic + '\'' +
                '}';
    }
}
