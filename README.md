
# producer

## Build
```bash
$ mvn package
```

## usage

```bash
$ java -cp $KAFKA_HOME/libs/kafka-clients-1.0.0.jar:$KAFKA_HOME/libs/slf4j-log4j12-1.7.25.jar:$KAFKA_HOME/libs/slf4j-api-1.7.25.jar:target/producer-0.0.1-SNAPSHOT.jar:$KAFKA_HOME/libs/log4j-1.2.17.jar:$SPARK_HOME/jars/commons-cli-1.2.jar org.nurnix.kafka.producer.main.SampleProducer -topic <grpname>
```
