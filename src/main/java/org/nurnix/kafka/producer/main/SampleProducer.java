package org.nurnix.kafka.producer.main;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SampleProducer {

    public static void main(String[] args) throws InterruptedException {

        System.out.println("Producer Sample");
        SampleProducer prog = new SampleProducer();
        prog.run(args);

    }

    private void run(String[] args) throws InterruptedException {

        ProducerOptionsParser parser = new ProducerOptionsParser();
        ProducerOptions popts = parser.parse(args);
        System.out.println(popts);

        String topic = popts.getTopic();

        Properties props = new Properties();
        try {
//            props.load(new FileInputStream("/home/martin/producer.properties"));
            props.load(getClass().getClassLoader().getResourceAsStream("producer.properties"));
            System.out.println(props.stringPropertyNames());
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*
        props.put("bootstrap.servers","cln01:9092");
        props.put("ack","all");
        props.put("retries",0);
        props.put("batch.size","16384");
        props.put("buffer.memory",33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        */
        Producer<String,String> producer = new KafkaProducer<>(props);
        for (int i=0;i<100;i++) {
            producer.send(new ProducerRecord<String,String>(topic,Integer.toString(i),Integer.toBinaryString(i)));
            Thread.sleep(10);
        }
        producer.close();
    }
}
