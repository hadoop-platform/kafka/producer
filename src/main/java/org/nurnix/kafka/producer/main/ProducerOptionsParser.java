package org.nurnix.kafka.producer.main;

import org.apache.commons.cli.*;

public class ProducerOptionsParser {
    private static final String cmdSchema = "java -cp ... org.nurnix.kafka.producer.main.SampleProducer <options>";
    private static final String ARG_TOPIC = "topic";

    private Options options = new Options();

    public ProducerOptionsParser() {
        registerOptions();
    }

    public ProducerOptions parse(String[] args) {
        CommandLineParser parser = new BasicParser();
        HelpFormatter formatter = new HelpFormatter();

        try {
            CommandLine cmd = parser.parse(options,args);
            return new ProducerOptions(cmd.getOptionValue(ARG_TOPIC));
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp(cmdSchema,options);
            System.exit(1);

        }
        return null;
    }


    private void registerOptions() {
        Option optionTopic = new Option(ARG_TOPIC,true,"Kafka topic");

        optionTopic.setRequired(true);

        options.addOption(optionTopic);

    }
}
